/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.bton.domain;

/**
 *
 * @author bobbyton
 */
public class City {
    
    public City(){
        
    }

    public City(int population, String district, String name, String countryCode) {
        this.population = population;
        this.district = district;
        this.name = name;
        this.countryCode = countryCode;
    }
    
    
    private int population;

    /**
     * Get the value of population
     *
     * @return the value of population
     */
    public int getPopulation() {
        return population;
    }

    /**
     * Set the value of population
     *
     * @param population new value of population
     */
    public void setPopulation(int population) {
        this.population = population;
    }

    
    private String district;

    /**
     * Get the value of district
     *
     * @return the value of district
     */
    public String getDistrict() {
        return district;
    }

    /**
     * Set the value of district
     *
     * @param district new value of district
     */
    public void setDistrict(String district) {
        this.district = district;
    }
    
    private String name;

    /**
     * Get the value of name
     *
     * @return the value of name
     */
    public String getName() {
        return name;
    }

    /**
     * Set the value of name
     *
     * @param name new value of name
     */
    public void setName(String name) {
        this.name = name;
    }

    private String countryCode;

    /**
     * Get the value of countryCode
     *
     * @return the value of countryCode
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * Set the value of countryCode
     *
     * @param countryCode new value of countryCode
     */
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    @Override
    public String toString() {
        return "City{" + "population=" + population + ", district=" + district + ", name=" + name + ", countryCode=" + countryCode + '}';
    }
    
}
