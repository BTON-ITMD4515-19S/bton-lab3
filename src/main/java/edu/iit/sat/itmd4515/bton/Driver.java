/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.bton;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author bobbyton
 */
public class Driver {
    public static void main(String[] args){
        String url = "jdbc:mysql://localhost:3306/world?zeroDateTimeBehavior=convertToNull";
        String username = "itmd4515";
        String password = "itmd4515";
        String query = "select * from country";
        
        try {
            
            Connection c = DriverManager.getConnection(url, username, password);
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(query);
            
            while(rs.next()){
                System.out.println(rs.getString("Code") + " " + rs.getString("Name"));
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(Driver.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
