/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.bton;

import edu.iit.sat.itmd4515.bton.domain.City;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import static org.junit.Assert.assertEquals;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author bobbyton
 */
public class CityClassTest {
    
    private static Validator validator;
    
    @BeforeClass
    public static void oneTimeSetupForTestClass(){
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }
    
    @Test
    public void testFailureBecauseNameIsNull(){
        City badCity = new City(1, "California", null, "USA");
        
        Set<ConstraintViolation<City>> constraintViolations = validator.validate(badCity);
        
      assertEquals( 1, constraintViolations.size() );
      assertEquals(
         "must not be blank",
         constraintViolations.iterator().next().getMessage()
      );
    }
}
